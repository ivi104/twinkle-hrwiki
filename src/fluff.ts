import { FluffCore, Preference, PreferenceGroup } from './core';

export class Fluff extends FluffCore {
	trustedBots = ['AnomieBOT', 'SineBot', 'MajavahBot'];

	static userPreferences() {
		const prefs = super.userPreferences() as PreferenceGroup;
		prefs.preferences = prefs.preferences.concat([
			{
				name: 'showRollbackLinks',
				label: 'Pokaži poveznice za vraćanje pri pregledu',
				type: 'set',
				setValues: {
					contribs: 'doprinosa',
					mine: 'mojih izmjena',
					others: 'tuđih izmjena',
					history: 'u povijesti izmjena',
					diff: 'razlikovnih poveznica (diff)',
					recent: 'nedavnih promjena',
				},
				default: ['diff', 'others'],
			},
			{
				name: 'confirmOnFluff',
				label: 'Traži potvrdu pri uklanjanju na računalu',
				helptip: 'helptip.',
				type: 'boolean',
				default: false,
			},
			{
				name: 'confirmOnMobileFluff',
				label: 'Traži potvrdu pri uklanjanju na mobitelu',
				helptip: 'helptip.',
				type: 'boolean',
				default: true,
			},
		] as Preference[]);
		return prefs;
	}
}
