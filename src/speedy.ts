import { SpeedyCore, criterion } from './core';
import { hatnoteRegex } from './common';

export class Speedy extends SpeedyCore {
	footerlinks = {
		'Speedy deletion policy': 'WP:CSD',
		'CSD prefs': 'WP:TW/PREF#speedy',
		'Twinkle help': 'WP:TW/DOC#speedy',
	};

	// {
	// 	label: 'G6: Move', //{{bris|move - page: page to be moved (G6) - reason: reason}}
	// 	value: 'move',
	// 	code: 'g6',
	// 	tooltip: 'Making way for an uncontroversial move like reversing a redirect',
	// 	subgroup: [
	// 		{
	// 			name: 'move_page',
	// 			parameter: 'page',
	// 			log: '[[:$1]]',
	// 			type: 'input',
	// 			label: 'Page to be moved here: ',
	// 		},
	// 		{
	// 			name: 'move_reason',
	// 			parameter: 'reason',
	// 			type: 'input',
	// 			label: 'Reason: ',
	// 			size: 60,
	// 		},
	// 	],
	// 	hideWhenMultiple: true,
	// 	},

	preprocessParamInputs() {
		let params = this.params; // shortcut reference
		if (params.banned_user) {
			params.banned_user = params.banned_user.replace(/^\s*User:/i, '');
		}
		if (params.redundantimage_filename) {
			params.redundantimage_filename = new mw.Title(params.redundantimage_filename, 6).toText();
		}
		if (params.commons_filename && params.commons_filename !== Morebits.pageNameNorm) {
			params.commons_filename = new mw.Title(params.commons_filename, 6).toText();
		}
	}

	validateInputs(): string | void {
		let input = this.params;
		let csd = new Set(input.csd); // optimise look-ups
		if (
			csd.has('userreq') &&
			mw.config.get('wgNamespaceNumber') === 3 &&
			!/\//.test(mw.config.get('wgTitle')) &&
			!input.userreq_rationale
		) {
			return 'CSD U1:  Please specify a rationale when nominating user talk pages.';
		}
		if (csd.has('repost') && input.repost_xfd && !/^(?:wp|wikipedia):/i.test(input.repost_xfd)) {
			return 'CSD G4:  The deletion discussion page name, if provided, must start with "Wikipedia:".';
		}
		if (csd.has('xfd') && input.xfd_fullvotepage && !/^(?:wp|wikipedia):/i.test(input.xfd_fullvotepage)) {
			return 'CSD G6 (XFD):  The deletion discussion page name, if provided, must start with "Wikipedia:".';
		}
		if (csd.has('imgcopyvio') && !input.imgcopyvio_url && !input.imgcopyvio_rationale) {
			return 'CSD F9: You must enter a url or reason (or both) when nominating a file under F9.';
		}
	}

	// Insert tag after short description or any hatnotes
	insertTagText(code, pageText) {
		let wikipage = new Morebits.wikitext.page(pageText);
		return wikipage.insertAfterTemplates(code + '\n', hatnoteRegex).getText();
	}

	criteriaLists: Array<{ label: string; visible: (self: Speedy) => boolean; list: Array<criterion> }> = [
		{
			label: 'Prilagođeni razlog',
			visible: (self) => !self.mode.isMultiple,
			list: [
				{
					label: 'Drugi razlog' + (Morebits.userIsSysop ? ' (drugi razlog)' : ' korištenje predloška db?'),
					value: '{{subst:void}}',
					code: 'drugo',
					tooltip:
						'{{db}} is short for "delete because". At least one of the other deletion criteria must still apply to the page, and you must make mention of this in your rationale. This is not a "catch-all" for when you can\'t find any criteria that fit.',
					subgroup: {
						name: 'bris-rucno',
						parameter: '1',
						utparam: '2',
						type: 'input',
						label: 'Razlog: ',
						size: 60,
					},
					hideWhenMultiple: true,
				},
			],
		},
		{
			label: 'Stranice za razgovor',
			// show on talk pages, but not user talk pages
			visible: (self) => self.namespace % 2 === 1 && self.namespace !== 3,
			list: [
				{
					label: 'SZR izbrisanog članka',
					value: 'SZR izbrisanog članka',
					code: '',
					tooltip:
						'Ne odnosi se na stranice korisne za projekt, poput suradničkih SZR, arhiva ili razgovora o datotekama koje postoje na Wikimedia Commonsu.',
				},
			],
		},
		{
			label: 'Datoteke',
			visible: (self) => !self.isRedirect && [6, 7].includes(self.namespace),
			list: [
				{
					label: 'Zahtjev postavljača',
					value: 'Zahtjev postavljača',
					code: '',
					tooltip: 'Brisanje zatražio suradnik koji je postavio datoteku',
				},
				{
					label: 'Kršenje autorskih prava',
					value: 'Kršenje autorskih prava',
					code: '',
					tooltip: 'Datoteka krši autorska prava',
				},
				{
					label: 'Kršenje Politike doktrine izuzetaka',
					value: 'Kršenje [[WP:PDI]]',
					code: '',
					tooltip: 'Slika je prevelika ili nepropisno atribuirana.',
					hideWhenMultiple: true,
				},
				{
					label: 'Više kopija iste datoteke',
					value: 'Više kopija iste datoteke',
					code: '',
					tooltip: 'Kopija ove ili slične datoteke već postoji u sustavu',
				},
				{
					label: 'Nekorištena datoteka',
					value: '[[Posebno:Nekorištene slike|nekorištena]] datoteka',
					code: '',
					tooltip: 'Datoteka se ne koristi a nije slobodna',
					hideWhenUser: true,
				},
				{
					label: 'Nepropisno postavljena datoteka',
					value: '[[Wikipedija:Slike|nepropisno]] postavljena datoteka, istekao rok za popravak',
					code: '',
					tooltip: 'Slika nema ispravno popunjen predložak Infoslika',
					hideWhenUser: true,
				},
				{
					label: 'Premješteno na Zajednički poslužitelj',
					value: 'premješteno na [[:commons:|zajednički poslužitelj]]',
					code: '',
					tooltip: 'Datoteka je premještena na Commons ili prihvaćena na OTRS-u.',
					hideWhenUser: true,
				},
				{
					label: 'Datoteka zamjenjena slobodnom datotekom',
					value: 'Datoteka zamjenjena slobodnom datotekom',
					code: '',
					tooltip: '.',
					subgroup: {
						name: 'zamjena-slobodnom',
						parameter: 'slobodna datoteka',
						type: 'input',
						label: 'Wikipoveznica do slobodne datoteke: ',
						size: 60,
					},
				},
			],
		},
		{
			label: 'Članci',
			visible: (self) => !self.isRedirect && [0, 1].includes(self.namespace),
			list: [
				{
					label: 'Strojni prijevod',
					value: 'Strojni prijevod',
					code: '',
					tooltip: 'tp',
				},
				{
					label: 'Članak na stranom jeziku',
					value: 'Članak na stranom jeziku',
					code: '',
					tooltip: 'tp',
				},
				{
					label: 'Prepisano bez dozvole',
					value: 'Prepisano bez dozvole ([[WP:DZ]])',
					code: '',
					tooltip: 'tp',
					hideWhenMultiple: true,
					subgroup: [
						{
							name: 'copyvio_url',
							parameter: 'url',
							utparam: 'url',
							type: 'input',
							label: 'URL (if available): ',
							tooltip:
								'If the material was copied from an online source, put the URL here, including the "http://" or "https://" protocol.',
							size: 60,
						},
						{
							name: 'copyvio_url2',
							parameter: 'url2',
							utparam: 'url2',
							type: 'input',
							label: 'Additional URL: ',
							tooltip: 'Optional. Should begin with "http://" or "https://"',
							size: 60,
						},
						{
							name: 'copyvio_url3',
							parameter: 'url3',
							utparam: 'url3',
							type: 'input',
							label: 'Additional URL: ',
							tooltip: 'Optional. Should begin with "http://" or "https://"',
							size: 60,
						},
					],
				},
				{
					label: 'Nedovoljan ili neuređen sadržaj',
					value: 'nedovoljan i/ili neuređen sadržaj ([[WP:MRVA]], [[WP:KUS]])',
					code: '',
					tooltip: '',
					hideWhenMultiple: true,
				},
				{
					label: 'Neodgovarajući sadržaj, istekao rok za popravak',
					value: 'Neodgovarajući sadržaj, istekao rok za popravak',
					code: '',
					tooltip: 'tp',
					hideWhenMultiple: true,
				},
				{
					label: 'Ne spada u Wikipediju',
					value: 'Ne spada u Wikipediju ([[WP:NIJE]])',
					code: '',
					tooltip: '',
					hideWhenMultiple: true,
				},
				{
					label: 'nepotrebna ili neprikladna razdvojba',
					value: 'nepotrebna ili neprikladna razdvojba)',
					code: '',
					tooltip: 'tp',
					hideWhenMultiple: true,
				},
				{
					label: 'članak postoji pod točnim nazivom',
					value: 'članak postoji pod točnim nazivom',
					code: '',
					tooltip: '',
					hideWhenMultiple: true,
				},
				{
					label: 'uklopljeno u drugi članak',
					value: 'uklopljeno u drugi članak',
					code: '',
					tooltip: '',
					hideWhenMultiple: true,
				},
				{
					label: 'netočne informacije',
					value: 'netočne informacije',
					code: '',
					tooltip: '',
					hideWhenMultiple: true,
				},
			],
		},
		{
			label: 'Kategorije',
			visible: (self) => !self.isRedirect && [14, 15].includes(self.namespace),
			list: [
				{
					label: 'Kategorija bez sadržaja',
					value: 'Kategorija bez sadržaja',
					code: '',
					tooltip:
						"Categories that have been unpopulated for at least seven days. This does not apply to categories being discussed at WP:CFD, disambiguation categories, and certain other exceptions. If the category isn't relatively new, it possibly contained articles earlier, and deeper investigation is needed",
				},
			],
		},
		{
			label: 'Suradničke stranice',
			visible: (self) => [2, 3].includes(self.namespace),
			list: [
				{
					label: 'Ne spada u suradničku stranicu',
					value: 'ne spada u suradničku stranicu ([[WP:SS]])',
					code: '',
					tooltip: 'tp',
				},
			],
		},
		{
			label: 'Općeniti kriteriji',
			visible: () => true,
			list: [
				{
					label: 'Vandalizam',
					value: 'Vandalizam ([[WP:VAN]])',
					code: '',
					tooltip: 'tp',
					// hideInNamespaces: [2], // Not applicable in userspace
				},
				{
					label: 'Grafit',
					value: 'grafit ([[Wikipedija:Rječnik#G|grafit]])',
					code: 'grafit',
					tooltip: 'tp',
				},
				{
					label: 'Reklama',
					value: 'Reklama ([[WP:REKLAMA]])',
					code: 'reklama',
					tooltip: '',
				},
				{
					label: 'Testiranje',
					value: 'Testiranje, koristite [[WP:SZV|stranicu za vježbanje]]',
					code: '',
					tooltip: 'tp',
					hideWhenMultiple: true,
				},
				{
					label: 'Stranica bez sadržaja',
					value: 'Stranica bez sadržaja',
					code: '',
					tooltip:
						'Article about a company or organization that does not assert the importance or significance of the subject',
					hideWhenMultiple: true,
				},
				{
					label: 'Autor zatražio brisanje ili izbijelio stranicu',
					value: 'Autor zatražio brisanje ili izbijelio stranicu',
					code: '',
					tooltip:
						"Any page for which deletion is requested by the original author in good faith, provided the page's only substantial content was added by its author. If the author blanks the page, this can also be taken as a deletion request.",
					subgroup: {
						name: 'author_rationale',
						parameter: 'razlog',
						type: 'input',
						label: 'Neobvezno pojašnjenje: ',
						tooltip: 'Perhaps linking to where the author requested this deletion.',
						size: 60,
					},
					hideSubgroupWhenSysop: true,
				},
				{
					label: 'Osobni napad (izbijeljuje stranicu)',
					value: 'Osobni napad',
					redactContents: true,
					code: '',
					tooltip:
						'Pages that serve no purpose but to disparage or threaten their subject or some other entity (e.g., "John Q. Doe is an imbecile"). This includes a biography of a living person that is negative in tone and unsourced, where there is no NPOV version in the history to revert to. Administrators deleting such pages should not quote the content of the page in the deletion summary!',
				},
				{
					label: 'Kršenje autorskih prava',
					value: 'Kršenje autorskih prava ([[WP:AP]])',
					code: '',
					tooltip: 'tp',
					subgroup: [
						{
							name: 'copyvio_url',
							parameter: 'url',
							utparam: 'url',
							type: 'input',
							label: 'Poveznica: ',
							tooltip:
								'If the material was copied from an online source, put the URL here, including the "http://" or "https://" protocol.',
							size: 60,
						},
						{
							name: 'copyvio_url2',
							parameter: 'url2',
							utparam: 'url2',
							type: 'input',
							label: 'Poveznica2: ',
							tooltip: 'Optional. Should begin with "http://" or "https://"',
							size: 60,
						},
						{
							name: 'copyvio_url3',
							parameter: 'url3',
							utparam: 'url3',
							type: 'input',
							label: 'Poveznica3: ',
							tooltip: 'Optional. Should begin with "http://" or "https://"',
							size: 60,
						},
					],
				},
				{
					label: 'Nacrt bez uređivanja u 6 mjeseci',
					value: 'Nacrt bez uređivanja u 6 mjeseci',
					code: 'g13',
					tooltip:
						'Any rejected or unsubmitted AfC submission in userspace or any non-redirect page in draft namespace, that has not been edited for more than 6 months. Blank drafts in either namespace are also included.',
					hideWhenRedirect: true,
					showInNamespaces: [2, 118], // user, draft namespaces only
				},
			],
		},
		{
			label: 'Redirects',
			visible: (self) => self.isRedirect,
			list: [
				{
					label: 'nepotrebno ili neprikladno preusmjeravanje ([[WP:PRE]])',
					value: 'nepotrebno ili neprikladno preusmjeravanje ([[WP:PRE]])',
					code: '',
					tooltip:
						'This only applies for orphaned disambiguation pages which either: (1) disambiguate only one existing Wikipedia page and whose title ends in "(disambiguation)" (i.e., there is a primary topic); or (2) disambiguate no (zero) existing Wikipedia pages, regardless of its title.  It also applies to orphan "Foo (disambiguation)" redirects that target pages that are not disambiguation or similar disambiguation-like pages (such as set index articles or lists)',
				},
				{
					label: 'Preusmjeravanje na nepostojeću stranicu',
					value: 'Preusmjeravanje na nepostojeću stranicu',
					code: '',
					tooltip:
						'This excludes any page that is useful to the project, and in particular: deletion discussions that are not logged elsewhere, user and user talk pages, talk page archives, plausible redirects that can be changed to valid targets, and file pages or talk pages for files that exist on Wikimedia Commons.',
					hideWhenMultiple: true,
				},
			],
		},
	];
}
