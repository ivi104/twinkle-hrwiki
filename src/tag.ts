import { Twinkle, Page, Config, Preference, PreferenceGroup, getPref } from './core';
import { TagCore, tagData, tagListType, TagMode, tagSubgroup } from './core';
import { hatnoteRegex } from './common';

const redirectTagList: tagListType = {};

const fileTagList: tagListType = {
	'Zajednički poslužitelj': [
		{ tag: 'Premjestiti na Commons', description: 'Datoteka je prikladna za premještanje na ZP' },
		{ tag: 'Galeriju na Commons', description: 'Galerija je prikladna za premještanje na ZP' },
		{ tag: 'NowCommons', description: 'Datoteka je premještena na ZP' },
		{ tag: 'NeZP', description: 'NE premjestiti datoteku na ZP' },
	],
	'Problemi s datotekom': [
		{
			tag: 'Preimenovati',
			description: 'Datoteku je pottrebno preimenovati',
			subgroup: [
				{
					type: 'input',
					name: 'slika-preimenovati-razlog',
					label: 'Razlog preimenovanja: ',
					parameter: '1=',
					size: '50',
				},
				{
					type: 'input',
					name: 'slika-preimenovati-naziv',
					label: 'Novo ime (ime.nastavak): ',
					parameter: '2=',
					tooltip: 'Nije potrebno pisati [[Datoteka:]], već samo ime',
					size: '50',
				},
			],
		},
	],
};

// Subgroups for {{merge}}, {{merge-to}} and {{merge-from}}
function getMergeSubgroups(tag: string): tagSubgroup[] {
	var otherTagName = 'Spajanje';
	switch (tag) {
		case 'Uklopi iz':
			otherTagName = 'Uklopi u';
			break;
		case 'Uklopi u':
			otherTagName = 'Uklopi iz';
			break;
		// no default
	}
	return ([
		{
			name: 'mergeTarget',
			parameter: '1',
			type: 'input',
			label: 'Ime drugog članka: ',
			tooltip: 'Obvezno.',
			required: true,
		},
		{
			type: 'checkbox',
			list: [
				{
					name: 'mergeTagOther',
					label: 'Označi drugi članak predloškom {{' + otherTagName + '}}',
					checked: true,
					tooltip: 'neobvezno',
				},
			],
		},
	] as tagSubgroup[]).concat(
		mw.config.get('wgNamespaceNumber') === 0
			? {
					name: 'mergeReason',
					type: 'textarea',
					label:
						'Razlog spajanja (bit će objavljeno na SZR ' +
						(tag === 'Uklopi u' ? 'drugog članka' : 'ovog članka') +
						'):',
					tooltip: 'Neobvezno, ali predloženo. Ostavite prazno ako ne želite navesti.',
			  }
			: []
	);
}

const articleTagList: tagListType = {
	//predlošci - https://hr.wikipedia.org/wiki/Kategorija:Predlo%C5%A1ci_-_odr%C5%BEavanje_Wikipedije
	'Problemi sa stranicom': [
		{
			tag: 'Bolji naslov',
			description: 'predlaže preimenovanje članka',
			subgroup: [
				{
					name: 'bolji-naslov-ime',
					parameter: '1',
					type: 'input',
					label: 'Prijedlog novog naslova: ',
					tooltip: 'Obvezno.',
					size: 35,
					required: true,
				},
				{
					name: 'bolji-naslov-razlog',
					parameter: 'r',
					type: 'input',
					label: 'Razlog: ',
					tooltip: 'neobvezno',
					size: 35,
				},
				{
					name: 'bolji-naslov-napomena',
					parameter: 'n',
					type: 'input',
					label: 'Napomena: ',
					tooltip: 'neobvezno',
					size: 35,
				},
			],
		},
		{
			tag: 'Nova razdvojba',
			description: 'predlaže se ovdje postaviti razdvojbenu stranicu',
			subgroup: {
				name: 'nova-razdvojba-razlog',
				parameter: 'r',
				type: 'input',
				label: 'Razlog: ',
				tooltip: 'neobvezno.',
				size: 35,
			},
		},
		{
			tag: 'Izdvoji',
			description: 'potrebno izdvajanje u drugi članak',
			subgroup: {
				name: 'izdvoji-ime',
				parameter: '1',
				type: 'input',
				label: 'Naslov novog članka: ',
				tooltip: 'neobvezno.',
				size: 35,
			},
		},
		{ tag: 'Podjela', description: 'potrebno podijeliti ili uklopiti sadržaj u postojeće članke' },
		{
			tag: 'Spajanje',
			description: 'predlaže spajanje članka s drugim',
			subgroup: getMergeSubgroups('Spajanje'),
		},
		{
			tag: 'Uklopi u',
			description: 'predlaže uklapanje sadržaja iz ovog u drugi člank',
			subgroup: getMergeSubgroups('Uklopi u'),
		},
		{
			tag: 'Uklopi iz',
			description: 'predlaže uklapanje sadržaja iz drugog članka u ovaj članak',
			subgroup: getMergeSubgroups('Uklopi iz'),
		},
		{
			tag: 'Uklopi iz nekoliko',
			description: 'predlaže uklapanje sadržaja iz nekoliko drugih članakaka',
			subgroup: [
				{
					name: 'uklopi-iz-1',
					parameter: '1',
					type: 'input',
					label: 'Ime 1. članka (samo ime): ',
					tooltip: 'neobvezno.',
					size: 35,
				},
				{
					name: 'uklopi-iz-2',
					parameter: '2',
					type: 'input',
					label: 'Ime 2. članka (samo ime): ',
					tooltip: 'neobvezno.',
					size: 35,
				},
				{
					name: 'uklopi-iz-3',
					parameter: '3',
					type: 'input',
					label: 'Ime 3. članka (samo ime): ',
					tooltip: 'neobvezno.',
					size: 35,
				},
				{
					name: 'uklopi-iz-4',
					parameter: '4',
					type: 'input',
					label: 'Ime 4. članka (samo ime): ',
					tooltip: 'neobvezno.',
					size: 35,
				},
				{
					name: 'uklopi-iz-4',
					parameter: '4',
					type: 'input',
					label: 'Ime 4. članka (samo ime): ',
					tooltip: 'neobvezno.',
					size: 35,
				},
				{
					name: 'uklopi-iz-5',
					parameter: '5',
					type: 'input',
					label: 'Ime 5. članka (samo ime): ',
					tooltip: 'neobvezno.',
					size: 35,
				},
			],
		},
		{ tag: 'Uklopi s razgovora', description: 'predlaže uklapanje sadržaja sa SZR' },
		{ tag: 'U wikicitat', description: 'članak treba premjestiti u wikicitat' },
		{ tag: 'U wikiknjige', description: 'članak treba premjestiti u wikiknjige' },
		{ tag: 'U wikizvor', description: 'članak treba premjestiti u wikizvor' },
		{ tag: 'U wječnik', description: 'članak treba premjestiti u wječnik' },
	],
	'Problemi sa sadržajem': [
		{ tag: 'Prekratko', description: 'članak je prekratak, čak i za mrvu' },
		{ tag: 'Priča', description: 'članak je pisan neenciklopedijski i zahtjeva skraćivanje' },
		{ tag: 'Esej', description: 'članak je pisan esejskim stilom' },
		{ tag: 'Sapunica', description: 'članak sadrži detaljne opise događaja u sapunici' },
		{ tag: 'Reklama', description: 'članak djeluje kao reklama' },
		{ tag: 'Rad obožavatelja', description: 'članak djeluje kao rad obožavatelja' },
		{ tag: 'Nedosljednost', description: 'članak navodi protuslovne činjenice' },
		{ tag: 'Jednostrano', description: 'članak pokriva samo jednu točku gledišta' },
		{ tag: 'Neutralnost', description: 'članak nije pisan nepristranom točkom gledišta' },
		{ tag: 'Neopravdana težina', description: 'manjinska gledišta imaju jednak prostor kao i široko prihvaćeni stavovi'	},
		{ tag: 'Originalne ideje', description: 'članak sadrži originalan istraživački rad' },
		{ tag: 'Neprimjereni zaključci', description: 'članak sadrži zaključke koji nisu sadržani u izvorima' },
		{ tag: 'Neprovjereno', description: 'članak navodi neprovjerene i nepouzdane podatke' },
		{ tag: 'Provjeriti', description: 'Potrebno je provjeriti točnost podataka u članku' },
		{ tag: 'Točnost', description: 'Točnost podataka u članku je osporena' },
		{ tag: 'Zastarjelo', description: 'Članak sadrži zastarjele podatke' },
	],
	'Problemi s izvorima i poveznicama': [
		{
			tag: 'Bolji izvori',
			description: 'potrebno navesti bolje izvore',
			subgroup: {
				name: 'bolji-izvori-napomena',
				parameter: '1',
				type: 'input',
				label: 'Napomena: ',
				tooltip: 'neobvezno.',
				size: 35,
			},
		},
		{ tag: 'Wikipedija nije sama sebi izvor', description: 'članak kao izvor koristi drugu Wikipediju' },
		{ tag: 'Primarni izvori', description: 'članak se previše oslanja na primarne izvore' },
		{ tag: 'Nedostaju izvori', description: 'članku nedostaju izvori' },
		{ tag: 'Nevjerodostojni izvori', description: 'članak sadrži izvore koji nisu vjerodostojni' },
		{ tag: 'Wikipedizirati izvore', description: 'izvore treba wikipedizirati' },
		{ tag: 'Dopuniti poveznice', description: 'neispravno ili nepotpuno napisane poveznice na izvore' },
		{ tag: 'URL i naslovi', description: 'gole poveznice bez naslova stranice' },
		{ tag: 'Provjeriti vanjske poveznice', description: 'članak zahtjeva provjeru i čišćenje vanjskih poveznica' },
		{ tag: 'Poveznice', description: 'pogrešno napisane wikipoveznice' },
	],
	'Problemi sa stilom pisanja': [
		{ tag: 'Formatirati tekst', description: 'potrebno je estetski urediti tekst (proredi, razmaci,...)' },
		{ tag: 'Wikipedizirati', description: 'tekst članka je potrebno preurediti u wikitekst' },
		{ tag: 'Wikipoveznice', description: 'tekst članka nema ili ima premalo wikipoveznica' },
		{ tag: 'Wp-', description: 'tekst članka ima previše wikipoveznica ili su one neodgovarajuće' },
		{ tag: 'Wp na hr!', description: 'tekst članka sadrži wikipoveznice prema drugim Wikipedijama' },
		{ tag: 'Decimalni zarez', description: 'u članku nije napisan decimalni zarez ondje gdje bi trebao biti' },
		{ tag: 'Odvajanje tisućica', description: 'u članku tisućice nisu odvojene točkom' },
		{ tag: 'Pogrješni format nadnevka', description: 'datum. mjesec u genitivu godina.' },
		{ tag: 'Kalendarske godine', description: 'godine nisu pisane kao redni brojevi (s točkom)' },
		{ tag: 'Stil-med', description: 'članak ne udovoljava stilskim odrednicama za područje medicine' },
		{ tag: 'Stilska dorada', description: 'formatirati kao Wikipedijin članak' },
		{ tag: 'Stil pisanja', description: 'članak potrebno napisati enciklopedijskim stilom' },
	],
	'Pravopis': [
		{ tag: 'Dijakritički znakovi', description: 'članku nedostaju dijakritici' },
		{ tag: 'Velika početna slova', description: 'u članku su neispravno napisana velika početna slova' },
		{ tag: 'Mala slova', description: 'članak je pisan s pretjeranom količinom malih slova' },
		{ tag: 'Velika slova', description: 'u članku se pretjerano koristi VERZAL' },
		{ tag: 'Pasive u aktive', description: 'članak je pisan s pretjeranom količinom pasivnih struktura' },
		{ tag: 'Pohrvatiti imena', description: 'potrebno je pohrvatiti imena toponima i osoba' },
		{ tag: 'Hrvatska imena vladara', description: 'potrebno je pohrvatiti imena vladara' },
		{ tag: 'Hrvatske kratice', description: 'potrebno je pohrvatiti kratice imena država' },
		{ tag: 'Nepotrebni anglizmi', description: 'potrebno je pohrvatiti anglizme' },
		{ tag: 'Neprikladno izražavanje', description: 'potrebno je ukloniti psovke i uvrjede' },
		{ tag: 'Sintaksa', description: 'članak nema red riječi u rečenici koji je u hrvatskom jeziku' },
		{ tag: 'Pravopis', description: 'članak zahtjeva jezičnu i pravopisnu doradu' },
	],
	'Izgubljeni u prijevodu': [
		{
			tag: 'Prijevod',
			description: 'članak zahtjeva prevođenje',
			subgroup: {
				name: 'prijevod',
				parameter: '1',
				type: 'input',
				label: 'troslovna kratica jezika: ',
				tooltip: 'npr. "eng" za engleski',
				size: 35,
				required: true,
			},
		},
		{ tag: 'Strojni prijevod', description: 'članak djeluje kao strojni prijevod' },
		{
			tag: 'Površan prijevod',
			description: 'članak je površno, nedovoljno ili netočbo preveden',
			subgroup: [
				{
					name: 'povrsan-prijevod-jezik',
					parameter: '1',
					type: 'input',
					label: 'jezik u nominativu: ',
					tooltip: 'npr. engleski',
					size: 35,
					required: true,
				},
				{
					name: 'povrsan-prijevvod-jezikGen',
					parameter: '2',
					type: 'input',
					label: 'jezik u genitivu: ',
					tooltip: 'npr. engleskog',
					size: 35,
				},
			],
		},
		{ tag: 'Površno prevedeno sa srpskog', description: 'članak je površno ili netočno preveden sa srpskog' },
		{ tag: 'Pravilno presloviti ćirilicu', description: 'potrebno je pravilno transliterirati ćirilična imena' },
	],
	'Potrebne dopune': [
		{ tag: 'Nema slika', description: 'potrebno je dodati medijske datoteke' },
		{
			tag: 'Dodaj infookvir',
			description: 'potrebno je dodati infookvir',
			subgroup: {
				name: 'dodaj-infookvir-ime',
				parameter: '1',
				type: 'input',
				label: 'Ime nedostajućeg infookvira: ',
				tooltip: 'neobvezno.',
				size: 35,
			},
		},
		{ tag: 'Dodaj infookvir anatomija', description: 'potrebno je dodati "Infookvir anatomija"' },
		{ tag: 'Dodaj taksokvir', description: 'potrebno je dodati predložak "Taksokvir"' },
	],
};

class ArticleMode extends TagMode {
	name = 'article';
	tagList = articleTagList;
	removalSupported = true;

	params: {
		newTags: string[];
		existingTags: string[];
		tagsToRemove: string[];
		tagsToRetain: string[];
		groupableExistingTags: string[];
		groupableNewTags: string[];
		nonGroupableNewTags: string[];
		groupableExistingTagsText: string;
		[paramName: string]: any;
	};

	// Configurations
	groupTemplateName = ''; // 'multiple issues' ,
	groupTemplateNameRegex = '(?:multiple ?issues|article ?issues|mi)(?!\\s*\\|\\s*section\\s*=)';
	groupTemplateNameRegexFlags = 'i';
	groupMinSize = 2;
	assumeUnknownTagsGroupable = false;

	static isActive() {
		return (
			[0, 2, 102].indexOf(mw.config.get('wgNamespaceNumber')) !== -1 && //NS main, user, dodatak, 118-draft
			!!mw.config.get('wgCurRevisionId') && // check if page exists
			!Morebits.isPageRedirect() //and is not redirect (see line 1280)
		);
	}

	getMenuTooltip() {
		return 'Dodaj ili ukloni predloške za održavanje';
	}

	getWindowTitle() {
		return 'Označavanje članka za održavanje';
	}

	makeForm(Window) {
		super.makeForm(Window);

		// this.form.append({
		// 	type: 'checkbox',
		// 	list: [
		// 		{
		// 			label: 'Group inside {{multiple issues}} if possible',
		// 			value: 'group',
		// 			name: 'group',
		// 			tooltip:
		// 				'If applying two or more templates supported by {{multiple issues}} and this box is checked, all supported templates will be grouped inside a {{multiple issues}} template.',
		// 			checked: getPref('groupByDefault'),
		// 		},
		// 	],
		// });

		this.form.append({
			type: 'input',
			label: 'Razlog',
			name: 'reason',
			tooltip: 'Neobvezni razlog koji će se pojaviti u sažetku. Preporućeno kod uklanjanja predložaka.',
			size: '60px',
		});

		this.formAppendPatrolLink();
	}

	// For historical reasons, this isn't named customArticleTagList
	getCustomTagPrefName() {
		return 'customTagList';
	}

	parseExistingTags() {
		this.existingTags = [];
		if (!this.canRemove()) {
			return;
		}

		// All tags are HTML table elements that are direct children of .mw-parser-output,
		// except when they are within {{multiple issues}}
		$('.mw-parser-output')
			.children()
			.each((i, e) => {
				// break out on encountering the first heading, which means we are no
				// longer in the lead section
				if (e.tagName === 'H2') {
					return false;
				}

				// The ability to remove tags depends on the template's {{ambox}} |name=
				// parameter bearing the template's correct name (preferably) or a name that at
				// least redirects to the actual name

				// All tags have their first class name as "box-" + template name
				if (e.className.indexOf('box-') === 0) {
					if (e.classList[0] === 'box-Multiple_issues') {
						$(e)
							.find('.ambox')
							.each((idx, e) => {
								var tag = e.classList[0].slice(4).replace(/_/g, ' ');
								this.existingTags.push(tag);
							});
						return; // continue
					}

					var tag = e.classList[0].slice(4).replace(/_/g, ' ');
					this.existingTags.push(tag);
				}
			});

		// {{Uncategorized}} and {{Improve categories}} are usually placed at the end
		if ($('.box-Uncategorized').length) {
			this.existingTags.push('Uncategorized');
		}
		if ($('.box-Improve_categories').length) {
			this.existingTags.push('Improve categories');
		}
	}

	// Tagging process:
	/// Initial cleanup
	/// Checking if group is present, or if it needs to be added
	/// Adding selected tags
	/// Putting existing tags into group if it's being added
	/// Removing unselected existing tags
	/// Final cleanup
	/// Save

	validateInput() {
		let params = this.params,
			tags = params.tags;
		if (['Spajanje', 'Uklopi iz', 'Uklopi u'].filter((t) => tags.includes(t)).length > 1) {
			return 'Molim odaberite samo jedan od predložaka {{Spajanje}}, {{Uklopi iz}} i {{Uklopi u}}. Ako je potrebno više spajanja, koristite predložak {{Uklopi iz nekoliko}}, i zatim ručno postavite {{Uklopi u}} na sve navedene članke.';
		}
		if ((params.mergeTagOther || params.mergeReason) && params.mergeTarget.indexOf('|') !== -1) {
			return 'Ako je potrebno više spajanja, koristite predložak {{Uklopi iz nekoliko}}, i zatim ručno postavite {{Uklopi u}} na sve navedene članke.';
		}
		if (
			['Prijevod', 'Strojni prijevod', 'Površan prijevod', 'Površno prevedeno sa srpskog'].filter((t) =>
				tags.includes(t)
			).length > 1
		) {
			return 'Molim odaberite samo jedan od predložaka: Prijevod, Strojni prijevod, Površan prijevod, Površno prevedeno sa srpskog.';
		}
	}

	preprocessParams() {
		super.preprocessParams();
		let params = this.params;

		params.disableGrouping = !params.group;

		params.tags.forEach((tag) => {
			switch (tag) {
				case 'Spajanje':
				case 'Uklopi u':
				case 'Uklopi iz':
					params.mergeTag = tag;
					// normalize the merge target for now and later
					params.mergeTarget = Morebits.string.toUpperCaseFirstChar(params.mergeTarget.replace(/_/g, ' '));

					this.templateParams[tag]['1'] = params.mergeTarget;

					// link to the correct section on the talk page, for article space only
					if (mw.config.get('wgNamespaceNumber') === 0 && (params.mergeReason || params.discussArticle)) {
						if (!params.discussArticle) {
							// discussArticle is the article whose talk page will contain the discussion
							params.discussArticle = tag === 'Uklopi u' ? params.mergeTarget : mw.config.get('wgTitle');
							// nonDiscussArticle is the article which won't have the discussion
							params.nonDiscussArticle = tag === 'Uklopi u' ? mw.config.get('wgTitle') : params.mergeTarget;
							var direction =
								'[[' +
								params.nonDiscussArticle +
								']]' +
								(params.mergeTag === 'Spajanje' ? ' sa ' : ' u ') +
								'[[' +
								params.discussArticle +
								']]';
							params.talkDiscussionTitleLinked = 'Prijedlog spajanja članka ' + direction;
							params.talkDiscussionTitle = params.talkDiscussionTitleLinked.replace(/\[\[(.*?)\]\]/g, '$1');
						}
						this.templateParams[tag].discuss = 'Talk:' + params.discussArticle + '#' + params.talkDiscussionTitle;
					}
					break;
				default:
					break;
			}
		});
	}

	initialCleanup() {
		this.pageText = this.pageText.replace(/\{\{\s*([Uu]serspace draft)\s*(\|(?:\{\{[^{}]*\}\}|[^{}])*)?\}\}\s*/g, '');
	}

	// getTagSearchRegex(tag) {
	// 	return new RegExp('\\{\\{' + tag + '(\\||\\}\\})', 'im');
	// }

	/**
	 * Create params.newTags, params.groupableNewTags, params.nonGroupableNewTags,
	 * params.groupableExistingTags
	 * Any tags to be added at the bottom of the page get added in this function itself.
	 */
	sortTags() {
		let params = this.params;
		params.newTags = params.tags.filter((tag) => {
			let exists = this.getTagRegex(tag).test(this.pageText);
			if (exists && (!this.flatObject[tag] || !this.flatObject[tag].dupeAllowed)) {
				Morebits.status.warn('Info', `Found {{${tag}}} on the ${this.name} already... excluding`);

				// XXX: don't do anything else with merge tags: handle this better!
				if (['Spajanje', 'Uklopi u'].indexOf(tag) !== -1) {
					params.mergeTarget = params.mergeReason = params.mergeTagOther = null;
				}
				return false; // remove from params.newTags
			} else if (tag === 'Uncategorized' || tag === 'Improve categories') {
				this.pageText += '\n\n' + this.getTagText(tag);
				return false; // remove from params.newTags, since it's now already inserted
			}
			return true;
		});

		if (!this.groupTemplateName) {
			// tag grouping disabled
			return;
		}

		params.groupableExistingTags = params.tagsToRetain.filter((tag) => this.isGroupable(tag));
		params.groupableNewTags = [];
		params.nonGroupableNewTags = [];
		params.newTags.forEach((tag) => {
			if (this.isGroupable(tag)) {
				params.groupableNewTags.push(tag);
			} else {
				params.nonGroupableNewTags.push(tag);
			}
		});
	}

	/**
	 * Adds new tags to pageText. If there are existing tags which are groupable but outside the
	 * group, they are put into it.
	 */
	addAndRearrangeTags() {
		let params = this.params;

		// Grouping disabled for this mode
		if (!this.groupTemplateName) {
			this.addTagsOutsideGroup(params.newTags);
			return $.Deferred().resolve();
		}

		/// Case 1. Group exists: New groupable tags put into group. Existing groupable tags that were outside are pulled in.
		if (this.groupRegex().test(this.pageText)) {
			Morebits.status.info('Info', 'Adding supported tags inside existing {{multiple issues}} tag');

			this.addTagsOutsideGroup(params.nonGroupableNewTags);

			// ensure all groupable existing tags are in group
			return this.spliceGroupableExistingTags().then((groupableExistingTagsText) => {
				this.addTagsIntoGroup(groupableExistingTagsText + this.makeTagSetText(params.groupableNewTags));
			});

			/// Case 2. No group exists, but should be added: Group created. Existing groupable tags are put in it. New groupable tags also put in it.
		} else if (this.shouldAddGroup()) {
			Morebits.status.info('Info', 'Grouping supported tags inside {{multiple issues}}');

			return this.spliceGroupableExistingTags().then((groupableExistingTagsText) => {
				let groupedTagsText =
					'{{' +
					this.groupTemplateName +
					'|\n' +
					this.makeTagSetText(params.groupableNewTags) +
					groupableExistingTagsText +
					'}}';
				let ungroupedTagsText = this.makeTagSetText(params.nonGroupableNewTags);
				this.pageText = this.insertTagText(groupedTagsText + '\n' + ungroupedTagsText, this.pageText);
			});

			/// Case 3. No group exists, no group to be added
		} else {
			this.addTagsOutsideGroup(params.newTags);
			return $.Deferred().resolve();
		}
		/// If group needs to be removed because of removal of tags, that's handled in finalCleanup, not here.
	}

	/**
	 * Inserts `tagText` (the combined wikitext of one or more tags) to the top of the
	 * pageText at the correct position, taking account of any existing hatnote templates.
	 * @param tagText
	 * @param pageText
	 */
	insertTagText(tagText: string, pageText: string) {
		// Insert tag after short description or any hatnotes,
		// as well as deletion/protection-related templates
		var wikipage = new Morebits.wikitext.page(pageText);
		var templatesAfter =
			hatnoteRegex +
			// Protection templates
			'pp|pp-.*?|' +
			// CSD
			'bris|NSBris|CBB?ris|KBris|SBris|Nacrt|nacrt|speedy deletion-.*?|' +
			// PROD
			'(?:proposed deletion|prod blp)\\/dated(?:\\s*\\|(?:concern|user|timestamp|help).*)+|' +
			// not a hatnote, but sometimes under a CSD or AfD
			'salt|proposed deletion endorsed';
		// AfD is special, as the tag includes html comments before and after the actual template
		// trailing whitespace/newline needed since this subst's a newline
		var afdRegex =
			'(?:<!--.*AfD.*\\n\\{\\{(?:Article for deletion\\/dated|AfDM).*\\}\\}\\n<!--.*(?:\\n<!--.*)?AfD.*(?:\\s*\\n))?';
		return wikipage.insertAfterTemplates(tagText, templatesAfter, null, afdRegex).getText();
	}

	// Override to include |date= param, which is applicable for all tags
	// Could also have done this by adding the date param for all tags in
	// this.templateParams thorough this.preprocessParams()
	getTagText(tag) {
		return '{{' + tag + this.getParameterText(tag) + '|date={{subst:CURRENTMONTHNAME}} {{subst:CURRENTYEAR}}}}';
	}

	savePage() {
		return super.savePage().then(() => {
			return this.postSave(this.pageobj);
		});
	}

	postSave(pageobj: Page) {
		let params = this.params;
		let promises = [];

		// special functions for merge tags
		if (params.mergeReason) {
			// post the rationale on the talk page (only operates in main namespace)
			var talkpage = new Page('Talk:' + params.discussArticle, 'Posting rationale on talk page');
			talkpage.setNewSectionText(params.mergeReason.trim() + ' ~~~~');
			talkpage.setNewSectionTitle(params.talkDiscussionTitleLinked);
			talkpage.setChangeTags(Twinkle.changeTags);
			talkpage.setWatchlist(getPref('watchMergeDiscussions'));
			talkpage.setCreateOption('recreate');
			promises.push(talkpage.newSection());
		}

		if (params.mergeTagOther) {
			// tag the target page if requested
			var otherTagName = 'Spajanje';
			if (params.mergeTag === 'Uklopi iz') {
				otherTagName = 'Uklopi u';
			} else if (params.mergeTag === 'Uklopi u') {
				otherTagName = 'Uklopi iz';
			}
			var otherpage = new Page(params.mergeTarget, 'Tagging other page (' + params.mergeTarget + ')');
			otherpage.setChangeTags(Twinkle.changeTags);
			promises.push(
				otherpage.load().then(() => {
					this.templateParams[otherTagName] = {
						// these will be accessed by this.getTagText()
						1: Morebits.pageNameNorm,
						discuss: this.templateParams[params.mergeTag].discuss || '',
					};
					// XXX: check if {{Merge from}} or {{Merge}} tag already exists?
					let pageText = this.insertTagText(this.getTagText(otherTagName) + '\n', otherpage.getPageText());
					otherpage.setPageText(pageText);
					otherpage.setEditSummary(TagCore.makeEditSummary([otherTagName], []));
					otherpage.setWatchlist(getPref('watchTaggedPages'));
					otherpage.setMinorEdit(getPref('markTaggedPagesAsMinor'));
					otherpage.setCreateOption('nocreate');
					return otherpage.save();
				})
			);
		}

		// post at WP:PNT for {{not English}} and {{rough translation}} tag
		if (params.translationPostAtPNT) {
			var pntPage = new Page(
				'Wikipedia:Pages needing translation into English',
				'Listing article at Wikipedia:Pages needing translation into English'
			);
			pntPage.setFollowRedirect(true);
			promises.push(
				pntPage.load().then(function friendlytagCallbacksTranslationListPage() {
					var old_text = pntPage.getPageText();

					var template = params.tags.indexOf('Rough translation') !== -1 ? 'duflu' : 'needtrans';
					var lang = params.translationLanguage;
					var reason = params.translationComments;

					var templateText =
						'{{subst:' +
						template +
						'|pg=' +
						Morebits.pageNameNorm +
						'|Language=' +
						(lang || 'uncertain') +
						'|Comments=' +
						reason.trim() +
						'}} ~~~~';

					var text, summary;
					if (template === 'duflu') {
						text = old_text + '\n\n' + templateText;
						summary = 'Translation cleanup requested on ';
					} else {
						text = old_text.replace(
							/\n+(==\s?Translated pages that could still use some cleanup\s?==)/,
							'\n\n' + templateText + '\n\n$1'
						);
						summary = 'Translation' + (lang ? ' from ' + lang : '') + ' requested on ';
					}

					if (text === old_text) {
						pntPage.getStatusElement().error('failed to find target spot for the discussion');
						return;
					}
					pntPage.setPageText(text);
					pntPage.setEditSummary(summary + ' [[:' + Morebits.pageNameNorm + ']]');
					pntPage.setChangeTags(Twinkle.changeTags);
					pntPage.setCreateOption('recreate');
					return pntPage.save();
				})
			);
		}

		if (params.translationNotify) {
			let statElem = new Morebits.status('Looking up creator');
			pageobj.setStatusElement(statElem);
			promises.push(
				pageobj.lookupCreation().then(function () {
					var initialContrib = pageobj.getCreator();
					statElem.info(`Found ${initialContrib}`);

					// Disallow warning yourself
					if (initialContrib === mw.config.get('wgUserName')) {
						statElem.warn('You (' + initialContrib + ') created this page; skipping user notification');
						return;
					}

					var userTalkPage = new Page(
						'User talk:' + initialContrib,
						'Notifying initial contributor (' + initialContrib + ')'
					);
					userTalkPage.setNewSectionTitle('Your article [[' + Morebits.pageNameNorm + ']]');
					userTalkPage.setNewSectionText(
						'{{subst:uw-notenglish|1=' +
							Morebits.pageNameNorm +
							(params.translationPostAtPNT ? '' : '|nopnt=yes') +
							'}} ~~~~'
					);
					userTalkPage.setEditSummary('Notice: Please use English when contributing to the English Wikipedia.');
					userTalkPage.setChangeTags(Twinkle.changeTags);
					userTalkPage.setCreateOption('recreate');
					userTalkPage.setFollowRedirect(true, false);
					return userTalkPage.newSection();
				})
			);
		}

		return $.when.apply($, promises);
	}
}

class FileMode extends TagMode {
	name = 'file';
	tagList = fileTagList;

	static isActive() {
		return (
			mw.config.get('wgNamespaceNumber') === 6 &&
			!document.getElementById('mw-sharedupload') &&
			!!document.getElementById('mw-imagepage-section-filehistory')
		);
	}

	getMenuTooltip() {
		return 'Dodaj ili ukloni predloške za održavanje';
	}

	getWindowTitle() {
		return 'Označavanje datoteka za održavanje';
	}

	makeForm(Window) {
		super.makeForm(Window);
		this.formAppendPatrolLink();
	}

	validateInput() {
		// Given an array of incompatible tags, check if we have two or more selected
		var params = this.params,
			tags = this.params.tags;

		let incompatibleSets = [['Premjestiti na Commons', 'Galeriju na Commons', 'NowCommons', 'NeZP']];
		for (let set of incompatibleSets) {
			if (set.filter((t) => tags.includes(t)).length > 1) {
				return 'Molim odaberite samo jedan od predložaka: {{' + set.join('}}, {{') + '}}.';
			}
		}

		// Get extension from either mime-type or title, if not present (e.g., SVGs)
		var extension =
			((extension = $('.mime-type').text()) && extension.split(/\//)[1]) ||
			mw.Title.newFromText(Morebits.pageNameNorm).getExtension();
		if (extension) {
			var extensionUpper = extension.toUpperCase();
			// What self-respecting file format has *two* extensions?!
			if (extensionUpper === 'JPG') {
				extension = 'JPEG';
			}

			// Check that selected templates make sense given the file's extension.

			// Bad GIF|JPEG|SVG
			var badIndex; // Keep track of where the offending template is so we can reference it below
			if (
				(extensionUpper !== 'GIF' && (badIndex = tags.indexOf('Bad GIF')) !== -1) ||
				(extensionUpper !== 'JPEG' && (badIndex = tags.indexOf('Bad JPEG')) !== -1) ||
				(extensionUpper !== 'SVG' && (badIndex = tags.indexOf('Bad SVG')) !== -1)
			) {
				var suggestion = 'This appears to be a ' + extension + ' file, ';
				if (['GIF', 'JPEG', 'SVG'].indexOf(extensionUpper) !== -1) {
					suggestion += 'please use {{Bad ' + extensionUpper + '}} instead.';
				} else {
					suggestion += 'so {{' + tags[badIndex] + '}} is inappropriate.';
				}
				return suggestion;
			}
			// Should be PNG|SVG
			if (tags.toString().indexOf('Should be ') !== -1 && tags.indexOf('Should be ' + extensionUpper) !== -1) {
				return 'This is already a ' + extension + ' file, so {{Should be ' + extensionUpper + '}} is inappropriate.';
			}

			// Overcompressed JPEG
			if (tags.indexOf('Overcompressed JPEG') !== -1 && extensionUpper !== 'JPEG') {
				return 'This appears to be a ' + extension + " file, so {{Overcompressed JPEG}} probably doesn't apply.";
			}
			// Bad trace and Bad font
			if (extensionUpper !== 'SVG') {
				if (tags.indexOf('Bad trace') !== -1) {
					return 'This appears to be a ' + extension + " file, so {{Bad trace}} probably doesn't apply.";
				} else if (tags.indexOf('Bad font') !== -1) {
					return 'This appears to be a ' + extension + " file, so {{Bad font}} probably doesn't apply.";
				}
			}
		}

		if (
			tags.indexOf('Do not move to Commons') !== -1 &&
			params.DoNotMoveToCommons_expiry &&
			(!/^2\d{3}$/.test(params.DoNotMoveToCommons_expiry) ||
				parseInt(params.DoNotMoveToCommons_expiry, 10) <= new Date().getFullYear())
		) {
			return 'Must be a valid future year.';
		}
	}

	initialCleanup() {
		this.params.tags.forEach((tag) => {
			switch (tag) {
				// when other commons-related tags are placed, remove "move to Commons" tag
				case 'Premjestiti na Commons':
				case 'Galeriju na Commons':
				case 'NowCommons':
				case 'NeZP':
					this.pageText = this.pageText.replace(
						/\{\{(premjestiti na commons|move to wikimedia commons|copy to wikimedia commons)[^}]*\}\}/gi,
						''
					);
					break;

				case 'Vector version available':
					this.pageText = this.pageText.replace(
						/\{\{((convert to |convertto|should be |shouldbe|to)?svg|badpng|vectorize)[^}]*\}\}/gi,
						''
					);
					break;

				case 'Orphaned non-free revisions':
					// remove {{non-free reduce}} and redirects
					this.pageText = this.pageText.replace(
						/\{\{\s*(Template\s*:\s*)?(Non-free reduce|FairUseReduce|Fairusereduce|Fair Use Reduce|Fair use reduce|Reduce size|Reduce|Fair-use reduce|Image-toobig|Comic-ovrsize-img|Non-free-reduce|Nfr|Smaller image|Nonfree reduce)\s*(\|(?:\{\{[^{}]*\}\}|[^{}])*)?\}\}\s*/gi,
						''
					);
					break;

				default:
					break;
			}
		});
	}
}

const draftTagList: tagListType = {
	'Nacrt': [
		{ tag: 'Nacrt', description: 'članak još nije spreman za GIP' },
	],
};

class DraftMode extends TagMode {
	name = 'draft';
	tagList = {...draftTagList, ...articleTagList};
	removalSupported = true;

	params: {
		newTags: string[];
		existingTags: string[];
		tagsToRemove: string[];
		tagsToRetain: string[];
		groupableExistingTags: string[];
		groupableNewTags: string[];
		nonGroupableNewTags: string[];
		groupableExistingTagsText: string;
		[paramName: string]: any;
	};

	// Configurations
	groupTemplateName = ''; // 'multiple issues' ,
	groupTemplateNameRegex = '(?:multiple ?issues|article ?issues|mi)(?!\\s*\\|\\s*section\\s*=)';
	groupTemplateNameRegexFlags = 'i';
	groupMinSize = 2;
	assumeUnknownTagsGroupable = false;

	static isActive() {
		return (
			mw.config.get('wgNamespaceNumber') === 118 && //draft
			!!mw.config.get('wgCurRevisionId') && // check if page exists
			!Morebits.isPageRedirect() //and is not redirect (see line 1280)
		);
	}

	getMenuTooltip() {
		return 'Dodaj ili ukloni predloške za održavanje';
	}

	getWindowTitle() {
		return 'Označavanje nacrta za održavanje';
	}

	makeForm(Window) {
		super.makeForm(Window);

		// this.form.append({
		// 	type: 'checkbox',
		// 	list: [
		// 		{
		// 			label: 'Group inside {{multiple issues}} if possible',
		// 			value: 'group',
		// 			name: 'group',
		// 			tooltip:
		// 				'If applying two or more templates supported by {{multiple issues}} and this box is checked, all supported templates will be grouped inside a {{multiple issues}} template.',
		// 			checked: getPref('groupByDefault'),
		// 		},
		// 	],
		// });

		this.form.append({
			type: 'input',
			label: 'Razlog',
			name: 'reason',
			tooltip: 'Neobvezni razlog koji će se pojaviti u sažetku. Preporućeno kod uklanjanja predložaka.',
			size: '60px',
		});

		this.formAppendPatrolLink();
	}

	// For historical reasons, this isn't named customArticleTagList
	getCustomTagPrefName() {
		return 'customTagList';
	}

	parseExistingTags() {
		this.existingTags = [];
		if (!this.canRemove()) {
			return;
		}

		// All tags are HTML table elements that are direct children of .mw-parser-output,
		// except when they are within {{multiple issues}}
		$('.mw-parser-output')
			.children()
			.each((i, e) => {
				// break out on encountering the first heading, which means we are no
				// longer in the lead section
				if (e.tagName === 'H2') {
					return false;
				}

				// The ability to remove tags depends on the template's {{ambox}} |name=
				// parameter bearing the template's correct name (preferably) or a name that at
				// least redirects to the actual name

				// All tags have their first class name as "box-" + template name
				if (e.className.indexOf('box-') === 0) {
					if (e.classList[0] === 'box-Multiple_issues') {
						$(e)
							.find('.ambox')
							.each((idx, e) => {
								var tag = e.classList[0].slice(4).replace(/_/g, ' ');
								this.existingTags.push(tag);
							});
						return; // continue
					}

					var tag = e.classList[0].slice(4).replace(/_/g, ' ');
					this.existingTags.push(tag);
				}
			});

		// {{Uncategorized}} and {{Improve categories}} are usually placed at the end
		if ($('.box-Uncategorized').length) {
			this.existingTags.push('Uncategorized');
		}
		if ($('.box-Improve_categories').length) {
			this.existingTags.push('Improve categories');
		}
	}

	// Tagging process:
	/// Initial cleanup
	/// Checking if group is present, or if it needs to be added
	/// Adding selected tags
	/// Putting existing tags into group if it's being added
	/// Removing unselected existing tags
	/// Final cleanup
	/// Save

	validateInput() {
		let params = this.params,
			tags = params.tags;
		if (['Spajanje', 'Uklopi iz', 'Uklopi u'].filter((t) => tags.includes(t)).length > 1) {
			return 'Molim odaberite samo jedan od predložaka {{Spajanje}}, {{Uklopi iz}} i {{Uklopi u}}. Ako je potrebno više spajanja, koristite predložak {{Uklopi iz nekoliko}}, i zatim ručno postavite {{Uklopi u}} na sve navedene članke.';
		}
		if ((params.mergeTagOther || params.mergeReason) && params.mergeTarget.indexOf('|') !== -1) {
			return 'Ako je potrebno više spajanja, koristite predložak {{Uklopi iz nekoliko}}, i zatim ručno postavite {{Uklopi u}} na sve navedene članke.';
		}
		if (
			['Prijevod', 'Strojni prijevod', 'Površan prijevod', 'Površno prevedeno sa srpskog'].filter((t) =>
				tags.includes(t)
			).length > 1
		) {
			return 'Molim odaberite samo jedan od predložaka: Prijevod, Strojni prijevod, Površan prijevod, Površno prevedeno sa srpskog.';
		}
	}

	preprocessParams() {
		super.preprocessParams();
		let params = this.params;

		params.disableGrouping = !params.group;

		params.tags.forEach((tag) => {
			switch (tag) {
				case 'Spajanje':
				case 'Uklopi u':
				case 'Uklopi iz':
					params.mergeTag = tag;
					// normalize the merge target for now and later
					params.mergeTarget = Morebits.string.toUpperCaseFirstChar(params.mergeTarget.replace(/_/g, ' '));

					this.templateParams[tag]['1'] = params.mergeTarget;

					// link to the correct section on the talk page, for article space only
					if (mw.config.get('wgNamespaceNumber') === 0 && (params.mergeReason || params.discussArticle)) {
						if (!params.discussArticle) {
							// discussArticle is the article whose talk page will contain the discussion
							params.discussArticle = tag === 'Uklopi u' ? params.mergeTarget : mw.config.get('wgTitle');
							// nonDiscussArticle is the article which won't have the discussion
							params.nonDiscussArticle = tag === 'Uklopi u' ? mw.config.get('wgTitle') : params.mergeTarget;
							var direction =
								'[[' +
								params.nonDiscussArticle +
								']]' +
								(params.mergeTag === 'Spajanje' ? ' sa ' : ' u ') +
								'[[' +
								params.discussArticle +
								']]';
							params.talkDiscussionTitleLinked = 'Prijedlog spajanja članka ' + direction;
							params.talkDiscussionTitle = params.talkDiscussionTitleLinked.replace(/\[\[(.*?)\]\]/g, '$1');
						}
						this.templateParams[tag].discuss = 'Talk:' + params.discussArticle + '#' + params.talkDiscussionTitle;
					}
					break;
				default:
					break;
			}
		});
	}

	initialCleanup() {
		this.pageText = this.pageText.replace(/\{\{\s*([Uu]serspace draft)\s*(\|(?:\{\{[^{}]*\}\}|[^{}])*)?\}\}\s*/g, '');
	}

	// getTagSearchRegex(tag) {
	// 	return new RegExp('\\{\\{' + tag + '(\\||\\}\\})', 'im');
	// }

	/**
	 * Create params.newTags, params.groupableNewTags, params.nonGroupableNewTags,
	 * params.groupableExistingTags
	 * Any tags to be added at the bottom of the page get added in this function itself.
	 */
	sortTags() {
		let params = this.params;
		params.newTags = params.tags.filter((tag) => {
			let exists = this.getTagRegex(tag).test(this.pageText);
			if (exists && (!this.flatObject[tag] || !this.flatObject[tag].dupeAllowed)) {
				Morebits.status.warn('Info', `Found {{${tag}}} on the ${this.name} already... excluding`);

				// XXX: don't do anything else with merge tags: handle this better!
				if (['Spajanje', 'Uklopi u'].indexOf(tag) !== -1) {
					params.mergeTarget = params.mergeReason = params.mergeTagOther = null;
				}
				return false; // remove from params.newTags
			} else if (tag === 'Uncategorized' || tag === 'Improve categories') {
				this.pageText += '\n\n' + this.getTagText(tag);
				return false; // remove from params.newTags, since it's now already inserted
			}
			return true;
		});

		if (!this.groupTemplateName) {
			// tag grouping disabled
			return;
		}

		params.groupableExistingTags = params.tagsToRetain.filter((tag) => this.isGroupable(tag));
		params.groupableNewTags = [];
		params.nonGroupableNewTags = [];
		params.newTags.forEach((tag) => {
			if (this.isGroupable(tag)) {
				params.groupableNewTags.push(tag);
			} else {
				params.nonGroupableNewTags.push(tag);
			}
		});
	}

	/**
	 * Adds new tags to pageText. If there are existing tags which are groupable but outside the
	 * group, they are put into it.
	 */
	addAndRearrangeTags() {
		let params = this.params;

		// Grouping disabled for this mode
		if (!this.groupTemplateName) {
			this.addTagsOutsideGroup(params.newTags);
			return $.Deferred().resolve();
		}

		/// Case 1. Group exists: New groupable tags put into group. Existing groupable tags that were outside are pulled in.
		if (this.groupRegex().test(this.pageText)) {
			Morebits.status.info('Info', 'Adding supported tags inside existing {{multiple issues}} tag');

			this.addTagsOutsideGroup(params.nonGroupableNewTags);

			// ensure all groupable existing tags are in group
			return this.spliceGroupableExistingTags().then((groupableExistingTagsText) => {
				this.addTagsIntoGroup(groupableExistingTagsText + this.makeTagSetText(params.groupableNewTags));
			});

			/// Case 2. No group exists, but should be added: Group created. Existing groupable tags are put in it. New groupable tags also put in it.
		} else if (this.shouldAddGroup()) {
			Morebits.status.info('Info', 'Grouping supported tags inside {{multiple issues}}');

			return this.spliceGroupableExistingTags().then((groupableExistingTagsText) => {
				let groupedTagsText =
					'{{' +
					this.groupTemplateName +
					'|\n' +
					this.makeTagSetText(params.groupableNewTags) +
					groupableExistingTagsText +
					'}}';
				let ungroupedTagsText = this.makeTagSetText(params.nonGroupableNewTags);
				this.pageText = this.insertTagText(groupedTagsText + '\n' + ungroupedTagsText, this.pageText);
			});

			/// Case 3. No group exists, no group to be added
		} else {
			this.addTagsOutsideGroup(params.newTags);
			return $.Deferred().resolve();
		}
		/// If group needs to be removed because of removal of tags, that's handled in finalCleanup, not here.
	}

	/**
	 * Inserts `tagText` (the combined wikitext of one or more tags) to the top of the
	 * pageText at the correct position, taking account of any existing hatnote templates.
	 * @param tagText
	 * @param pageText
	 */
	insertTagText(tagText: string, pageText: string) {
		// Insert tag after short description or any hatnotes,
		// as well as deletion/protection-related templates
		var wikipage = new Morebits.wikitext.page(pageText);
		var templatesAfter =
			hatnoteRegex +
			// Protection templates
			'pp|pp-.*?|' +
			// CSD
			'bris|NSBris|CBB?ris|KBris|SBris|Nacrt|nacrt|speedy deletion-.*?|' +
			// PROD
			'(?:proposed deletion|prod blp)\\/dated(?:\\s*\\|(?:concern|user|timestamp|help).*)+|' +
			// not a hatnote, but sometimes under a CSD or AfD
			'salt|proposed deletion endorsed';
		// AfD is special, as the tag includes html comments before and after the actual template
		// trailing whitespace/newline needed since this subst's a newline
		var afdRegex =
			'(?:<!--.*AfD.*\\n\\{\\{(?:Article for deletion\\/dated|AfDM).*\\}\\}\\n<!--.*(?:\\n<!--.*)?AfD.*(?:\\s*\\n))?';
		return wikipage.insertAfterTemplates(tagText, templatesAfter, null, afdRegex).getText();
	}

	// Override to include |date= param, which is applicable for all tags
	// Could also have done this by adding the date param for all tags in
	// this.templateParams thorough this.preprocessParams()
	getTagText(tag) {
		return '{{' + tag + this.getParameterText(tag) + '|date={{subst:CURRENTMONTHNAME}} {{subst:CURRENTYEAR}}}}';
	}

	savePage() {
		return super.savePage().then(() => {
			return this.postSave(this.pageobj);
		});
	}

	postSave(pageobj: Page) {
		let params = this.params;
		let promises = [];

		// special functions for merge tags
		if (params.mergeReason) {
			// post the rationale on the talk page (only operates in main namespace)
			var talkpage = new Page('Talk:' + params.discussArticle, 'Posting rationale on talk page');
			talkpage.setNewSectionText(params.mergeReason.trim() + ' ~~~~');
			talkpage.setNewSectionTitle(params.talkDiscussionTitleLinked);
			talkpage.setChangeTags(Twinkle.changeTags);
			talkpage.setWatchlist(getPref('watchMergeDiscussions'));
			talkpage.setCreateOption('recreate');
			promises.push(talkpage.newSection());
		}

		if (params.mergeTagOther) {
			// tag the target page if requested
			var otherTagName = 'Spajanje';
			if (params.mergeTag === 'Uklopi iz') {
				otherTagName = 'Uklopi u';
			} else if (params.mergeTag === 'Uklopi u') {
				otherTagName = 'Uklopi iz';
			}
			var otherpage = new Page(params.mergeTarget, 'Tagging other page (' + params.mergeTarget + ')');
			otherpage.setChangeTags(Twinkle.changeTags);
			promises.push(
				otherpage.load().then(() => {
					this.templateParams[otherTagName] = {
						// these will be accessed by this.getTagText()
						1: Morebits.pageNameNorm,
						discuss: this.templateParams[params.mergeTag].discuss || '',
					};
					// XXX: check if {{Merge from}} or {{Merge}} tag already exists?
					let pageText = this.insertTagText(this.getTagText(otherTagName) + '\n', otherpage.getPageText());
					otherpage.setPageText(pageText);
					otherpage.setEditSummary(TagCore.makeEditSummary([otherTagName], []));
					otherpage.setWatchlist(getPref('watchTaggedPages'));
					otherpage.setMinorEdit(getPref('markTaggedPagesAsMinor'));
					otherpage.setCreateOption('nocreate');
					return otherpage.save();
				})
			);
		}

		// post at WP:PNT for {{not English}} and {{rough translation}} tag
		if (params.translationPostAtPNT) {
			var pntPage = new Page(
				'Wikipedia:Pages needing translation into English',
				'Listing article at Wikipedia:Pages needing translation into English'
			);
			pntPage.setFollowRedirect(true);
			promises.push(
				pntPage.load().then(function friendlytagCallbacksTranslationListPage() {
					var old_text = pntPage.getPageText();

					var template = params.tags.indexOf('Rough translation') !== -1 ? 'duflu' : 'needtrans';
					var lang = params.translationLanguage;
					var reason = params.translationComments;

					var templateText =
						'{{subst:' +
						template +
						'|pg=' +
						Morebits.pageNameNorm +
						'|Language=' +
						(lang || 'uncertain') +
						'|Comments=' +
						reason.trim() +
						'}} ~~~~';

					var text, summary;
					if (template === 'duflu') {
						text = old_text + '\n\n' + templateText;
						summary = 'Translation cleanup requested on ';
					} else {
						text = old_text.replace(
							/\n+(==\s?Translated pages that could still use some cleanup\s?==)/,
							'\n\n' + templateText + '\n\n$1'
						);
						summary = 'Translation' + (lang ? ' from ' + lang : '') + ' requested on ';
					}

					if (text === old_text) {
						pntPage.getStatusElement().error('failed to find target spot for the discussion');
						return;
					}
					pntPage.setPageText(text);
					pntPage.setEditSummary(summary + ' [[:' + Morebits.pageNameNorm + ']]');
					pntPage.setChangeTags(Twinkle.changeTags);
					pntPage.setCreateOption('recreate');
					return pntPage.save();
				})
			);
		}

		if (params.translationNotify) {
			let statElem = new Morebits.status('Looking up creator');
			pageobj.setStatusElement(statElem);
			promises.push(
				pageobj.lookupCreation().then(function () {
					var initialContrib = pageobj.getCreator();
					statElem.info(`Found ${initialContrib}`);

					// Disallow warning yourself
					if (initialContrib === mw.config.get('wgUserName')) {
						statElem.warn('You (' + initialContrib + ') created this page; skipping user notification');
						return;
					}

					var userTalkPage = new Page(
						'User talk:' + initialContrib,
						'Notifying initial contributor (' + initialContrib + ')'
					);
					userTalkPage.setNewSectionTitle('Your article [[' + Morebits.pageNameNorm + ']]');
					userTalkPage.setNewSectionText(
						'{{subst:uw-notenglish|1=' +
							Morebits.pageNameNorm +
							(params.translationPostAtPNT ? '' : '|nopnt=yes') +
							'}} ~~~~'
					);
					userTalkPage.setEditSummary('Notice: Please use English when contributing to the English Wikipedia.');
					userTalkPage.setChangeTags(Twinkle.changeTags);
					userTalkPage.setCreateOption('recreate');
					userTalkPage.setFollowRedirect(true, false);
					return userTalkPage.newSection();
				})
			);
		}

		return $.when.apply($, promises);
	}
}

// Override to change modes available,
// each mode is a class extending TagMode
TagCore.modeList = [
	// RedirectMode, // keep RedirectMode above ArticleMode TODO
	ArticleMode,
	FileMode,
	DraftMode,
];

export class Tag extends TagCore {
	footerlinks = {
		'Twinkle help': ':en:WP:TW/DOC#tag',
	};

	static userPreferences() {
		const prefs = super.userPreferences() as PreferenceGroup;
		prefs.preferences = prefs.preferences.concat([
			{
				name: 'watchTaggedVenues',
				label: 'Add page to watchlist when tagging these type of pages',
				type: 'set',
				setValues: { articles: 'Articles', drafts: 'Drafts', redirects: 'Redirects', files: 'Files' },
				default: ['articles', 'drafts', 'redirects', 'files'],
			},
			{
				name: 'watchMergeDiscussions',
				label: 'Add talk pages to watchlist when starting merge discussions',
				type: 'enum',
				enumValues: Config.watchlistEnums,
			},
			{
				name: 'groupByDefault',
				label: 'Check the "group into {{multiple issues}}" box by default',
				type: 'boolean',
				default: true,
			},
			{
				name: 'customTagList',
				label: 'Custom article/draft maintenance tags to display',
				helptip:
					"These appear as additional options at the bottom of the list of tags. For example, you could add new maintenance tags which have not yet been added to Twinkle's defaults.",
				type: 'customList',
				customListValueTitle: 'Template name (no curly brackets)',
				customListLabelTitle: 'Text to show in Tag dialog',
				default: [],
			},
			{
				name: 'customFileTagList',
				label: 'Custom file maintenance tags to display',
				helptip: 'Additional tags that you wish to add for files.',
				type: 'customList',
				customListValueTitle: 'Template name (no curly brackets)',
				customListLabelTitle: 'Text to show in Tag dialog',
				default: [],
			},
			{
				name: 'customRedirectTagList',
				label: 'Custom redirect category tags to display',
				helptip: 'Additional tags that you wish to add for redirects.',
				type: 'customList',
				customListValueTitle: 'Template name (no curly brackets)',
				customListLabelTitle: 'Text to show in Tag dialog',
				default: [],
			},
		] as Preference[]);
		return prefs;
	}
}
