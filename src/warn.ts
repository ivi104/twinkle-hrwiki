import { WarnCore, obj_entries, warningLevel, warning, getPref } from './core';

export class Warn extends WarnCore {
	footerlinks = {
		'Choosing a warning level': 'WP:UWUL#Levels',
		'Warn prefs': 'WP:TW/PREF#warn',
		'Twinkle help': 'WP:TW/DOC#warn',
		'Give feedback': 'WT:TW',
	};

	warningLevels: Record<
		string,
		{ label: string; summaryPrefix?: string; selected: (pref: number) => boolean; visible?: () => boolean }
	> = {
		'level1': {
			label: '1: Napomena',
			selected: (pref) => pref === 1,
		},
		'level2': {
			label: '2: Obavijest',
			selected: (pref) => pref === 2,
		},
		'level!': {
			label: '!: Upozorenje',
			selected: (pref) => pref === 3,
		},
		'singlenotice': {
			label: 'Jednostruke obavijesti',
			selected: (pref) => pref === 6,
			visible: () => !getPref('combinedSingletMenus'),
		},
		'singlewarn': {
			label: 'Jednostruka upozorenja',
			selected: (pref) => pref === 7,
			visible: () => !getPref('combinedSingletMenus'),
		},
		'singlecombined': {
			label: 'Jednostruke poruke',
			selected: (pref) => pref === 6 || pref === 7,
			visible: () => !!getPref('combinedSingletMenus'),
		},
		'kitchensink': {
			label: 'Svi predlošci',
			selected: (pref) => pref === 10,
		},
		// unsupported
		'autolevel': {
			label: 'Auto-odabir razine (1-3)',
			selected: (pref) => pref === 11,
		},
		'custom': {
			label: 'Ručna upozorenja',
			selected: (pref) => pref === 9,
			visible: () => !!getPref('customWarningList')?.length,
		},
	};

	processWarnings() {
		type MessageConfig = {
			label: string;
			summary: string;
			heading?: string;
			suppressArticleInSummary?: true;
		};
		type MessagesType = {
			levels: Record<string, Record<string, Record<string, MessageConfig>>>;
			singlenotice: Record<string, MessageConfig>;
			singlewarn: Record<string, MessageConfig>;
		};

		const messages: MessagesType = {
			levels: {
				'Česta upozorenja': {
					'NS-vandalizam': {
						'level1': {
							label: 'Vandalizam',
							summary: 'Napomena: Unconstructive editing',
						},
						'level2': {
							label: 'Vandalizam',
							summary: 'Obavijest: Unconstructive editing',
						},
						'level!': {
							label: 'Vandalizam',
							summary: 'Upozorenje: Vandalism',
						},
					},
					'NS-test': {
						'level1': {
							label: 'Testiranje',
							summary: 'Napomena: Unconstructive editing',
						},
						'level2': {
							label: 'Testiranje',
							summary: 'Obavijest: Unconstructive editing',
						},
						'level!': {
							label: 'Testiranje',
							summary: 'Upozorenje: Disruptive editing',
						},
					},
					'NS-jezik': {
						'level1': {
							label: 'Jezik',
							summary: 'Napomena: Unconstructive editing',
						},
						'level2': {
							label: 'Jezik',
							summary: 'Obavijest: Unconstructive editing',
						},
						'level!': {
							label: 'Jezik',
							summary: 'Upozorenje: Disruptive editing',
						},
					},
				},
				'Ponašanje u člancima': {
					'NS-prikaži': {
						'level1': {
							label: 'Prikaži',
							summary: 'Napomena: Unconstructive editing',
						},
						'level2': {
							label: 'Prikaži',
							summary: 'Obavijest: Unconstructive editing',
						},
						'level!': {
							label: 'Prikaži',
							summary: 'Upozorenje: Disruptive editing',
						},
					},
					'NS-bris': {
						'level1': {
							label: 'Brisanje sadržaja',
							summary: 'Napomena: Unconstructive editing',
						},
						'level2': {
							label: 'Brisanje sadržaja',
							summary: 'Obavijest: Unconstructive editing',
						},
						'level!': {
							label: 'Brisanje sadržaja',
							summary: 'Upozorenje: Disruptive editing',
						},
					},
					'NS-netočno': {
						'level1': {
							label: 'Netočno',
							summary: 'Napomena: Unconstructive editing',
						},
						'level2': {
							label: 'Netočno',
							summary: 'Obavijest: Unconstructive editing',
						},
						'level!': {
							label: 'Netočno',
							summary: 'Upozorenje: Disruptive editing',
						},
					},
					'NS-izvori': {
						'level1': {
							label: 'Izvori',
							summary: 'Napomena: Unconstructive editing',
						},
						'level2': {
							label: 'Izvori',
							summary: 'Obavijest: Unconstructive editing',
						},
						'level!': {
							label: 'Izvori',
							summary: 'Upozorenje: Disruptive editing',
						},
					},
				},
				'Promocija i spam': {},
				'Ponašanje prema drugima': {},
				'Slike': {
					'NS-slike': {
						'level1': {
							label: 'Slike',
							summary: 'Napomena: Unconstructive editing',
						},
						'level2': {
							label: 'Slike',
							summary: 'Obavijest: Unconstructive editing',
						},
						'level!': {
							label: 'Slike',
							summary: 'Upozorenje: Vandalism',
						},
					},
				},
				'Drugo': {
					'NS-razgovor': {
						'level1': {
							label: 'Razgovor',
							summary: 'Napomena: Unconstructive editing',
						},
						'level2': {
							label: 'Razgovor',
							summary: 'Obavijest: Unconstructive editing',
						},
						'level!': {
							label: 'Razgovor',
							summary: 'Upozorenje: Vandalism',
						},
					},
				},
			},
			singlenotice: {},
			singlewarn: {},
		};

		let groupObject: warningLevel['list'] = {
			'Česta upozorenja': [],
			'Ponašanje u člancima': [],
			'Promocija i spam': [],
			'Ponašanje prema drugima': [],
			'Slike': [],
			'Drugo': [],
		};

		let groups: Record<string, warningLevel> = {
			'level1': { label: '1: General note', list: $.extend(true, {}, groupObject) },
			'level2': { label: '2: Caution', list: $.extend(true, {}, groupObject) },
			'level!': { label: '3: Warning', list: $.extend(true, {}, groupObject) },
		};

		if (getPref('combinedSingletMenus')) {
			groups.singlecombined = {
				label: 'Single-issue messages',
				list: obj_entries(messages.singlenotice)
					.concat(obj_entries(messages.singlewarn))
					.sort((a, b) => (a.name < b.name ? -1 : 1))
					.map(([name, data]) => {
						return $.extend(
							{
								template: name,
							},
							data
						);
					}),
			};
		} else {
			groups.singlenotice = {
				label: 'Singe-issue notices',
				list: obj_entries(messages.singlenotice).map(([name, data]) => {
					return $.extend(
						{
							template: name,
						},
						data
					);
				}),
			};
			groups.singlewarn = {
				label: 'Single-issue warnings',
				list: obj_entries(messages.singlewarn).map(([name, data]) => {
					return $.extend(
						{
							template: name,
						},
						data
					);
				}),
			};
		}

		for (let [subgroupName, templateSet] of obj_entries(messages.levels)) {
			for (let [templateName, templateLevels] of obj_entries(templateSet)) {
				for (let [level, templateData] of obj_entries(templateLevels)) {
					groups[level].list[subgroupName].push(
						$.extend(
							{
								template: templateName + level.slice('level'.length),
							},
							templateData
						)
					);
				}
			}
		}

		this.warnings = groups;
	}

	getWarningWikitext(templateName, article, reason, isCustom) {
		var text = '{{subst:' + templateName;

		// add linked article for user warnings
		if (article) {
			// c&pmove has the source as the first parameter
			if (templateName === 'uw-c&pmove') {
				text += '|to=' + article;
			} else {
				text += '|1=' + article;
			}
		}
		if (reason && !isCustom) {
			// add extra message
			if (
				templateName === 'uw-csd' ||
				templateName === 'uw-probation' ||
				templateName === 'uw-userspacenoindex' ||
				templateName === 'uw-userpage'
			) {
				text += "|3=''" + reason + "''";
			} else {
				text += "|2=''" + reason + "''";
			}
		}
		text += '}}';

		if (reason && isCustom) {
			// we assume that custom warnings lack a {{{2}}} parameter
			text += " ''" + reason + "''";
		}

		return text + ' ~~~~';
	}

	validateInputs(params) {
		if (params.sub_group === 'uw-username' && !params.article) {
			return 'You must supply a reason for the {{uw-username}} template.';
		}
	}

	getHistoryRegex(): RegExp {
		return /<!--\s?Template:([uU]w-.*?)\s?-->.*?(\d{1,2}:\d{1,2}, \d{1,2} \w+ \d{4} \(UTC\))/g;
	}

	getInputConfig(template: string) {
		let input = super.getInputConfig(template);

		// Tags that don't take a linked article, but something else (often a username).
		// The value of each tag is the label next to the input field
		switch (template) {
			case 'uw-agf-sock':
				input.label = 'Optional username of other account (without User:) ';
				input.className = 'userInput';
				break;
			case 'uw-bite':
				input.label = "Username of 'bitten' user (without User:) ";
				input.className = 'userInput';
				break;
			case 'uw-socksuspect':
				input.label = 'Username of sock master, if known (without User:) ';
				input.className = 'userInput';
				break;
			case 'uw-username':
				input.label = 'Username violates policy because... ';
				break;
			case 'uw-aiv':
				input.label = 'Optional username that was reported (without User:) ';
				input.className = 'userInput';
				break;
			// no default
		}

		return input;
	}

	customiseSummaryWithInput(summary: string, input: string, messageData: warning) {
		// these templates require a username
		if (['uw-agf-sock', 'uw-socksuspect', 'uw-aiv'].includes(messageData.template)) {
			return summary + ' of [[:User:' + input + ']]';
		}
		return super.customiseSummaryWithInput(summary, input, messageData);
	}

	perWarningNotices(template): JQuery {
		switch (template) {
			case 'uw-username':
				return $(
					"<div style='color: red;' id='tw-warn-red-notice'>{{uw-username}} should <b>not</b> be used for <b>blatant</b> username policy violations. " +
						"Blatant violations should be reported directly to UAA (via Twinkle's ARV tab). " +
						'{{uw-username}} should only be used in edge cases in order to engage in discussion with the user.</div>'
				);
			case 'uw-coi-username':
				return $(
					"<div style='color: red;' id='tw-warn-red-notice'>{{uw-coi-username}} should <b>not</b> be used for <b>blatant</b> username policy violations. " +
						"Blatant violations should be reported directly to UAA (via Twinkle's ARV tab). " +
						'{{uw-coi-username}} should only be used in edge cases in order to engage in discussion with the user.</div>'
				);
			default:
				return $();
		}
	}
}
