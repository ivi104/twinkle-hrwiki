import { Twinkle, TwinkleModule, getPref, addPortletLink } from './core';

export class Welcome extends TwinkleModule {
	moduleName = 'welcome';
	static moduleName = 'welcome';

	constructor() {
		super();
		if (mw.util.getParamValue('friendlywelcome')) {
			if (mw.util.getParamValue('friendlywelcome') === 'auto') {
				Welcome.auto();
			} else {
				Welcome.semiauto();
			}
		} else {
			Welcome.normal();
		}
	}

	static auto() {
		if (mw.util.getParamValue('action') !== 'edit') {
			// userpage not empty, aborting auto-welcome
			return;
		}

		Welcome.welcomeUser();
	}

	static semiauto() {
		Welcome.callback(mw.config.get('wgRelevantUserName'));
	}

	static normal() {
		if (mw.util.getParamValue('diff')) {
			// check whether the contributors' talk pages exist yet
			var $oList = $('#mw-diff-otitle2').find('span.mw-usertoollinks a.new:contains(razgovor)').first();
			var $nList = $('#mw-diff-ntitle2').find('span.mw-usertoollinks a.new:contains(razgovor)').first();

			if ($oList.length > 0 || $nList.length > 0) {
				var spanTag = function (color, content) {
					var span = document.createElement('span');
					span.style.color = color;
					span.appendChild(document.createTextNode(content));
					return span;
				};

				var welcomeNode = document.createElement('strong');
				var welcomeLink = document.createElement('a');
				welcomeLink.appendChild(spanTag('Black', '['));
				welcomeLink.appendChild(spanTag('Goldenrod', 'welcome'));
				welcomeLink.appendChild(spanTag('Black', ']'));
				welcomeNode.appendChild(welcomeLink);

				if ($oList.length > 0) {
					var oHref = $oList.attr('href');

					var oWelcomeNode = welcomeNode.cloneNode(true);
					oWelcomeNode.firstChild.setAttribute(
						'href',
						oHref +
							'&' +
							$.param({
								friendlywelcome: getPref('quickWelcomeMode') === 'auto' ? 'auto' : 'norm',
								vanarticle: Morebits.pageNameNorm,
							})
					);
					$oList[0].parentNode.parentNode.appendChild(document.createTextNode(' '));
					$oList[0].parentNode.parentNode.appendChild(oWelcomeNode);
				}

				if ($nList.length > 0) {
					var nHref = $nList.attr('href');

					var nWelcomeNode = welcomeNode.cloneNode(true);
					nWelcomeNode.firstChild.setAttribute(
						'href',
						nHref +
							'&' +
							$.param({
								friendlywelcome: getPref('quickWelcomeMode') === 'auto' ? 'auto' : 'norm',
								vanarticle: Morebits.pageNameNorm,
							})
					);
					$nList[0].parentNode.parentNode.appendChild(document.createTextNode(' '));
					$nList[0].parentNode.parentNode.appendChild(nWelcomeNode);
				}
			}
		}
		// Users and IPs but not IP ranges
		if (mw.config.exists('wgRelevantUserName') && !Morebits.ip.isRange(mw.config.get('wgRelevantUserName'))) {
			addPortletLink(
				function () {
					Welcome.callback(mw.config.get('wgRelevantUserName'));
				},
				'Dobrodošlica',
				'twinkle-welcome',
				'Pošalji dobrodošlicu suradniku'
			);
		}
	}

	static welcomeUser() {
		Morebits.status.init(document.getElementById('mw-content-text'));
		$('#catlinks').remove();

		var params = {
			template: getPref('quickWelcomeTemplate'),
			article: mw.util.getParamValue('vanarticle') || '',
			mode: 'auto',
		};

		var userTalkPage = mw.config.get('wgFormattedNamespaces')[3] + ':' + mw.config.get('wgRelevantUserName');
		Morebits.wiki.actionCompleted.redirect = userTalkPage;
		Morebits.wiki.actionCompleted.notice = 'Welcoming complete, reloading talk page in a few seconds';

		var wikipedia_page = new Morebits.wiki.page(userTalkPage, 'User talk page modification');
		wikipedia_page.setFollowRedirect(true);
		wikipedia_page.setCallbackParameters(params);
		wikipedia_page.load(Welcome.callbacks.main);
	}

	static callback(uid) {
		if (uid === mw.config.get('wgUserName') && !confirm('Are you really sure you want to welcome yourself?…')) {
			return;
		}

		var Window = new Morebits.simpleWindow(600, 420);
		Window.setTitle('Welcome user');
		Window.setScriptName('Twinkle');
		Window.addFooterLink('Welcoming Committee', 'WP:WC');
		Window.addFooterLink('Welcome postavke', 'WP:Twinkle/Postavke#welcome');
		Window.addFooterLink('Twinkle help', 'WP:TW/DOC#welcome');
		Window.addFooterLink('Give feedback', 'WT:TW');

		var form = new Morebits.quickForm(Welcome.evaluate);

		form.append({
			type: 'select',
			name: 'type',
			label: 'Type of welcome: ',
			event: Welcome.populateWelcomeList,
			list: [
				{
					type: 'option',
					value: 'standard',
					label: 'Standardne dobrodošlice',
					selected: !mw.util.isIPAddress(mw.config.get('wgRelevantUserName')),
				},
				{
					type: 'option',
					value: 'anonymous',
					label: 'Dobrodošlice IP suradnicima',
					selected: mw.util.isIPAddress(mw.config.get('wgRelevantUserName')),
				},
			],
		});

		form.append({
			type: 'div',
			id: 'welcomeWorkArea',
			className: 'morebits-scrollbox',
		});

		form.append({
			type: 'input',
			name: 'article',
			label: '* Linked article (if supported by template):',
			value: mw.util.getParamValue('vanarticle') || '',
			tooltip:
				'An article might be linked from within the welcome if the template supports it. Leave empty for no article to be linked.  Templates that support a linked article are marked with an asterisk.',
		});

		var previewlink = document.createElement('a');
		$(previewlink).click(function () {
			Welcome.callbacks.preview(result); // |result| is defined below
		});
		previewlink.style.cursor = 'pointer';
		previewlink.textContent = 'Preview';
		form.append({ type: 'div', name: 'welcomepreview', label: [previewlink] });

		form.append({ type: 'submit' });

		var result = form.render();
		Window.setContent(result);
		Window.display();

		// initialize the welcome list
		var evt = document.createEvent('Event');
		evt.initEvent('change', true, true);
		result.type.dispatchEvent(evt);
	}

	static populateWelcomeList(e) {
		var type = e.target.value;

		var container = new Morebits.quickForm.element({ type: 'fragment' });

		/* 		if ((type === 'standard' || type === 'anonymous') && getPref('customWelcomeList').length) {
			container.append({ type: 'header', label: 'Ručni predlošci dobrodošlice (promjenjivo u postavkama)' });
			container.append({
				type: 'radio',
				name: 'template',
				list: getPref('customWelcomeList'),
				event: function () {
					e.target.form.article.disabled = false;
				},
			});
		} */

		var sets = Welcome.templates[type];
		$.each(sets, function (label, templates) {
			container.append({ type: 'header', label: label });
			container.append({
				type: 'radio',
				name: 'template',
				list: $.map(templates, function (properties, template) {
					return {
						value: template,
						label: '{{' + template + '}}: ' + properties.description + (properties.linkedArticle ? '\u00A0*' : ''), // U+00A0 NO-BREAK SPACE
						tooltip: properties.tooltip, // may be undefined
					};
				}),
				event: function (ev) {
					ev.target.form.article.disabled = !templates[ev.target.value].linkedArticle;
				},
			});
		});

		var rendered = container.render();
		$(e.target.form).find('div#welcomeWorkArea').empty().append(rendered);

		var firstRadio = e.target.form.template[0];
		firstRadio.checked = true;
		var vals = sets[Object.keys(sets)[0]];
		e.target.form.article.disabled = vals[firstRadio.value] ? !vals[firstRadio.value].linkedArticle : true;
	}

	// A list of welcome templates and their properties and syntax

	// The four fields that are available are "description", "linkedArticle", "syntax", and "tooltip".
	// The three magic words that can be used in the "syntax" field are:
	//   - $USERNAME$  - replaced by the welcomer's username, depending on user's preferences
	//   - $ARTICLE$   - replaced by an article name, if "linkedArticle" is true
	//   - $HEADER$    - adds a level 2 header (most templates already include this)
	static templates = {
		standard: {
			'Zadani predlošci dobrodošlice': {
				'subst:Dd': {
					description: 'Dobrodošlica',
					linkedArticle: false,
					syntax: '{{subst:dd}}~~~~',
				},
				'subst:Dobrodošlica-eng1': {
					description: 'Dobrodošlica za suradnike na engleskom jeziku',
					linkedArticle: false,
					syntax: '{{subst:Dobrodošlica-eng1|~~~~}}',
				},
			},
		},

		anonymous: {
			'Dobrodošlice za anonimne suradnike': {
				'subst:NS-sič1': {
					description: 'Potiče anonimnog suradnika na otvaranje računa',
					linkedArticle: true,
					syntax: '{{subst:NS-sič1|1=$ARTICLE$}} ~~~~',
				},
			},
		},
	};

	static getTemplateWikitext(type, template, article) {
		// the iteration is required as the type=standard has two groups
		var properties;
		$.each(Welcome.templates[type], function (label, templates) {
			properties = templates[template];
			if (properties) {
				return false; // break
			}
		});
		if (properties) {
			return properties.syntax
				.replace('$USERNAME$', getPref('insertUsername') ? mw.config.get('wgUserName') : '')
				.replace('$ARTICLE$', article ? article : '')
				.replace(/\$HEADER\$\s*/, '== Welcome ==\n\n')
				.replace('$EXTRA$', ''); // EXTRA is not implemented yet
		}
		return (
			'{{subst:' +
			template +
			(article ? '|art=' + article : '') +
			'}}' +
			(getPref('customWelcomeSignature') ? ' ~~~~' : '')
		);
	}

	static callbacks = {
		preview: function (form) {
			var previewDialog = new Morebits.simpleWindow(750, 400);
			previewDialog.setTitle('Welcome template preview');
			previewDialog.setScriptName('Welcome user');
			previewDialog.setModality(true);

			var previewdiv = document.createElement('div');
			previewdiv.style.marginLeft = previewdiv.style.marginRight = '0.5em';
			previewdiv.style.fontSize = 'small';
			previewDialog.setContent(previewdiv);

			var previewer = new Morebits.wiki.preview(previewdiv);
			var input = Morebits.quickForm.getInputData(form);
			previewer.beginRender(
				Welcome.getTemplateWikitext(input.type, input.template, input.article),
				'User talk:' + mw.config.get('wgRelevantUserName')
			); // Force wikitext/correct username

			var submit = document.createElement('input');
			submit.setAttribute('type', 'submit');
			submit.setAttribute('value', 'Close');
			previewDialog.addContent(submit);

			previewDialog.display();

			$(submit).click(function () {
				previewDialog.close();
			});
		},
		main: function (pageobj) {
			var params = pageobj.getCallbackParameters();
			var text = pageobj.getPageText();

			// abort if mode is auto and form is not empty
			if (pageobj.exists() && params.mode === 'auto') {
				Morebits.status.info(
					'Upozorenje',
					'Suradnička stranica za razgovor nije prazna; prekidam automatsko slanje dobrodošlice'
				);
				Morebits.wiki.actionCompleted.event();
				return;
			}

			var welcomeText = Welcome.getTemplateWikitext(params.type, params.template, params.article);

			if (getPref('topWelcomes')) {
				text = welcomeText + '\n\n' + text;
			} else {
				text += '\n' + welcomeText;
			}

			var summaryText = 'Dobro došli!';
			pageobj.setPageText(text);
			pageobj.setEditSummary(summaryText);
			pageobj.setChangeTags(Twinkle.changeTags);
			pageobj.setWatchlist(getPref('watchWelcomes'));
			pageobj.setCreateOption('recreate');
			pageobj.save();
		},
	};

	static evaluate(e) {
		var form = e.target;

		var params = Morebits.quickForm.getInputData(form); // : type, template, article
		params.mode = 'manual';

		Morebits.simpleWindow.setButtonsEnabled(false);
		Morebits.status.init(form);

		var userTalkPage = mw.config.get('wgFormattedNamespaces')[3] + ':' + mw.config.get('wgRelevantUserName');
		Morebits.wiki.actionCompleted.redirect = userTalkPage;
		Morebits.wiki.actionCompleted.notice = 'Welcoming complete, reloading talk page in a few seconds';

		var wikipedia_page = new Morebits.wiki.page(userTalkPage, 'User talk page modification');
		wikipedia_page.setFollowRedirect(true);
		wikipedia_page.setCallbackParameters(params);
		wikipedia_page.load(Welcome.callbacks.main);
	}
}
